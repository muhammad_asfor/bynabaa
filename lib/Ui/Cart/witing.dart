import 'dart:async';
import 'package:flutter/material.dart';

import 'package:dio/dio.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:tajra/Ui/Cart/CheckoutSuccessPage.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'CheckoutSuccessPage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/cart_bloc.dart';

class Witing extends StatefulWidget {
  bool id;
  Witing({Key? key, required this.id}) : super(key: key);
  @override
  WitingState createState() => WitingState();
}

class WitingState extends State<Witing> {
  checkout(bool id) async {
    try {
      if (id) {
        BlocProvider.of<CartBloc>(context).add(GetCartEvent());

        await Future.delayed(const Duration(milliseconds: 200), () {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (_) => CheckoutSuccessPage()));
        });
      } else {
        Navigator.pop(
          context,
        );
        AppSnackBar.show(
            context, "حدثت مشكلة اثناء عملية الدفع", ToastType.Error);
      }
    } on DioError catch (e) {
      Navigator.pop(
        context,
      );
    } catch (e) {
      Navigator.pop(
        context,
      );
    }
  }

  @override
  void initState() {
    checkout(widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }
}
