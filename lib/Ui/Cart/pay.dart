import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:tajra/Ui/Cart/witing.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewExample extends StatefulWidget {
  String urlpay;
  WebViewExample({Key? key, required this.urlpay}) : super(key: key);
  @override
  WebViewExampleState createState() => WebViewExampleState();
}

bool isLoading = true;

class WebViewExampleState extends State<WebViewExample> {
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid)
      WebView.platform = SurfaceAndroidWebView(); // <<== THIS
  }

  @override
  Widget build(BuildContext context) {
    Completer<WebViewController> _controller = Completer<WebViewController>();
    Future<bool> onWillPop() async {
      return Future.value(true);
    }

    return WillPopScope(
      onWillPop: onWillPop,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          body: Stack(children: [
            WebView(
              initialUrl: widget.urlpay,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller.complete(webViewController);
              },
              onProgress: (int progress) {
                print('WebView is loading (progress : $progress%)');
              },
              onPageFinished: (finish) {
                setState(() {
                  isLoading = false;
                });
              },
              onPageStarted: (String url) {
                setState(() {
                  isLoading = true;
                });
                if (url.split("orders/")[0] == "https://bynabaa.com/") {
                  if (url.split("complete/")[0] ==
                      "https://bynabaa.com/orders/") {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Witing(
                                id: true,
                              )),
                    );
                  } else {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Witing(
                                id: false,
                              )),
                    );
                  }
                }
              },
              gestureNavigationEnabled: true,
              backgroundColor: const Color(0x00000000),
            ),
            isLoading
                ? Container(
                    color: Colors.white,
                    width: double.infinity,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : Container(),
          ]),
        ),
      ),
    );
  }
}
